# Asset Static Assets

This is a repository being used with a CDN service for web assets on AMS websites.

[https://rawgit.org/](RawGit)

### To Use The CDN:

Combine the base path with the file permalink to get the full URL

Base path: `https://glcdn.rawgit.org/asset-marketing-systems/static-assets`

###### Example:

`https://glcdn.rawgit.org/asset-marketing-systems/static-assets` 

appended with

`/4ad95654b18b68753d510e7bac35d463481878c4/logos/svg/asset_logo_solid_aqua.svg` 

links to

[https://glcdn.rawgit.org/asset-marketing-systems/static-assets/4ad95654b18b68753d510e7bac35d463481878c4/logos/svg/asset_logo_solid_aqua.svg](https://glcdn.rawgit.org/asset-marketing-systems/static-assets/4ad95654b18b68753d510e7bac35d463481878c4/logos/svg/asset_logo_solid_aqua.svg)
